package com.howtodoinjava.demo.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.howtodoinjava.demo.exception.RecordNotFoundException;
import com.howtodoinjava.demo.model.ArticleEntity;
import com.howtodoinjava.demo.repository.ArticleRepository;

@Service
public class ArticleService {
	
	@Autowired
	ArticleRepository repository;
	
	public List<ArticleEntity> getAllArticle()
	{
		List<ArticleEntity> result = (List<ArticleEntity>) repository.findAll();
		
		if(result.size() > 0) {
			return result;
		} else {
			return new ArrayList<ArticleEntity>();
		}
	}
	
	public ArticleEntity getArticleById(Long id) throws RecordNotFoundException
	{
		Optional<ArticleEntity> employee = repository.findById(id);
		
		if(employee.isPresent()) {
			return employee.get();
		} else {
			throw new RecordNotFoundException("No article record exist for given id");
		}
	}

	public ArticleEntity createOrUpdateArticle(ArticleEntity entity)
	{
		if(entity.getId()  == null)
		{
			entity = repository.save(entity);

			return entity;
		}
		else
		{
			Optional<ArticleEntity> articleEntity = repository.findById(entity.getId());

			if(articleEntity.isPresent())
			{
				ArticleEntity newEntity = articleEntity.get();
				newEntity.setTitle(entity.getTitle());
				newEntity.setDescription(entity.getDescription());
				newEntity.setImg(entity.getImg());

				newEntity = repository.save(newEntity);

				return newEntity;
			} else {
				entity = repository.save(entity);

				return entity;
			}
		}
	}

	public void deleteArticleById(Long id) throws RecordNotFoundException
	{
		Optional<ArticleEntity> articleEntity = repository.findById(id);

		if(articleEntity.isPresent())
		{
			repository.deleteById(id);
		} else {
			throw new RecordNotFoundException("No article record exist for given id");
		}
	}
}